import Vue from 'vue'
import Vuex from 'vuex'

let cart = window.localStorage.getItem('cart');
let cartReturn = window.localStorage.getItem('cartReturn');

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    /* User */
    userName: null,
    userEmail: null,
    userAvatar: null,

    /* NavBar */
    isNavBarVisible: true,

    /* FooterBar */
    isFooterBarVisible: true,

    /* Aside */
    isAsideVisible: true,
    isAsideMobileExpanded: false,

    /* Cart */
    cart: cart ? JSON.parse(cart) : [],
    client: null,
    /* Cart Return */
    cartReturn: cartReturn ? JSON.parse(cartReturn) : [],
  },
  mutations: {
    /* Add to Cart */
    addToCart(state, book) {
      state.cart.push(book);
      this.commit('saveCart');
    },

    /* Add to Return Cart */
    addToCartReturn(state, booksArray) {
      state.cartReturn = booksArray;
      console.log(booksArray);
      console.log(state.cartReturn);
      this.commit('saveCartReturn');
    },

    /* Remove From Cart */
    removeFromCart(state, book) {
      let index = state.cart.indexOf(book);

      state.cart.splice(index, 1);

      this.commit('saveCart');
    },

    /* Remove From Return Cart */
    removeFromCartReturn(state, book) {
      state.cartReturn = state.cartReturn.filter(function(item){
        return item.pivot.id !== book.pivot.id;
      });

      this.commit('saveCartReturn');
    },

    /* Clear cart after checkout */

    clearCartAfterCheckout(state) {
      console.log('fuuuuck this shit')
      state.cart = [];
      this.commit('saveCart');
    },

    // /* Clear cart after Return */

    // clearCartAfterReturn(state) {
    //   state.cartReturn = [];
    //   this.commit('saveCartReturn');
    // },

    /* Save Cart */
    saveCart(state) {
      window.localStorage.setItem('cart', JSON.stringify(state.cart));
    },
    /* Save Cart Return */
    saveCartReturn(state) {
      window.localStorage.setItem('cartReturn', JSON.stringify(state.cartReturn));
    },

    /* Set Client */
    setClient(state, payload){
      state.client = payload.client
    },

    /* A fit-them-all commit */
    basic (state, payload) {
      state[payload.key] = payload.value
    },

    /* User */
    user (state, payload) {
      if (payload.name) {
        state.userName = payload.name
      }
      if (payload.email) {
        state.userEmail = payload.email
      }
      if (payload.avatar) {
        state.userAvatar = payload.avatar
      }
    },

    /* Aside Mobile */
    asideMobileStateToggle (state, payload = null) {
      const htmlClassName = 'has-aside-mobile-expanded'

      let isShow

      if (payload !== null) {
        isShow = payload
      } else {
        isShow = !state.isAsideMobileExpanded
      }

      if (isShow) {
        document.documentElement.classList.add(htmlClassName)
      } else {
        document.documentElement.classList.remove(htmlClassName)
      }

      state.isAsideMobileExpanded = isShow
    }
  },
  actions: {

  }
})
