import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/return',
      name: 'return',
      component: () => import('./views/Return.vue')
    },
    {
      path: '/books',
      name: 'books',
      component: () => import('./views/Books.vue')
    },
    {
      path: '/forms',
      name: 'forms',
      component: () => import('./views/Forms.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/clients/index',
      name: 'clients.index',
      component: () => import('./views/Clients/ClientsIndex.vue'),
    },
    {
      path: '/clients/new',
      name: 'clients.new',
      component: () => import('./views/Clients/ClientsForm.vue'),
    },
    {
      path: '/clients/:id',
      name: 'clients.edit',
      component: () => import('./views/Clients/ClientsForm.vue'),
      props: true
    },
    {
      path: '/books/index',
      name: 'books.index',
      component: () => import('./views/Books/BooksIndex.vue'),
    },
    {
      path: '/books/new',
      name: 'books.new',
      component: () => import('./views/Books/BooksForm.vue'),
    },
    {
      path: '/books/:id',
      name: 'books.edit',
      component: () => import('./views/Books/BooksForm.vue'),
      props: true
    },
    {
      path: '/categories/index',
      name: 'categories.index',
      component: () => import('./views/Categories/CategoriesIndex.vue'),
    },
    {
      path: '/categories/new',
      name: 'categories.new',
      component: () => import('./views/Categories/CategoriesForm.vue'),
    },
    {
      path: '/categories/:id',
      name: 'categories.edit',
      component: () => import('./views/Categories/CategoriesForm.vue'),
      props: true
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
