<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/data','HomeController@data')->name('home-data');

/*
 * Clients management
 * */
Route::prefix('/clients')->group(function () {
    Route::get('', 'ClientsController@index');
    Route::get('{client}', 'ClientsController@show')->name('clients.show');
    Route::post('store', 'ClientsController@store');
    Route::patch('{client}', 'ClientsController@update');
    Route::post('destroy', 'ClientsController@destroyMass');
    Route::delete('{client}/destroy', 'ClientsController@destroy');
});

/*
 * Books management
 * */
Route::prefix('/books')->group(function () {
    Route::get('', 'BooksController@index');
    Route::get('/search', 'BooksController@search');
    Route::get('{book}', 'BooksController@show')->name('books.show');
    Route::post('store', 'BooksController@store');
    Route::patch('{book}', 'BooksController@update');
    Route::post('destroy', 'BooksController@destroyMass');
    Route::delete('{book}/destroy', 'BooksController@destroy');


});
/* Book search
*
*/
Route::get('search','WebController@search');
Route::post('books-checkout','BooksController@borrow');

/*
 * Return management
 * */

Route::delete('clientbook/{clientbook}/destroy', 'ClientBookController@destroy');
Route::get('clientbook', 'ClientBookController@index');



Route::prefix('/categories')->group(function () {
    Route::get('', 'CategoryController@index');
    Route::get('{category}', 'CategoryController@show');
    Route::post('store', 'CategoryController@store');
    Route::patch('{category}', 'CategoryController@update');
    Route::post('destroy', 'CategoryController@destroyMass');
    Route::delete('{category}/destroy', 'CategoryController@destroy');
});
/*
 * Current user
 * */
Route::prefix('/user')->group(function () {
    Route::get('', 'CurrentUserController@show');
    Route::patch('', 'CurrentUserController@update');
    Route::patch('/password', 'CurrentUserController@updatePassword');
});

/*
 * File upload (e.g. avatar)
 * */
Route::post('/files/store', 'FilesController@store');
