<?php

namespace App;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model implements Searchable
{

    public function getSearchResult(): SearchResult
    {       $url = route('books.show', $this->id);

       return new SearchResult($this, $this->name, $url);
    }

    use SoftDeletes;

    public function categories(){
        return $this->belongsToMany(Category::class,'category_book');
    }
    public function clients(){
        return $this->belongsToMany(Client::class,'clients_books');
    }

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'file'
    ];

    protected $fillable = [
        'name',
        'author',
        'published_date',
        'availability',
        'publisher'

    ];
}
