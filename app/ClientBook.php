<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientBook extends Model
{
    //

    use SoftDeletes;

    protected $table = 'clients_books';


}
