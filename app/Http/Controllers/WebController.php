<?php

namespace App\Http\Controllers;

use App\Book;
use Spatie\Searchable\Search;
use App\Models\Category;
use Illuminate\Http\Request;

class WebController extends Controller
{
    //
    public function attach(Request $request)
    {

        $book = Book::find($request->get('book_id'));
        $book->category()->attach($request->get('category_id'));

        return redirect('/');
    }

    public function search(Request $request)
{
    $results = (new Search())
    ->registerModel(Book::class, ['name', 'author'])
    ->search($request->input('query'));

    return response()->json($results);
  }
}
