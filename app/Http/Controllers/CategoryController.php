<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryStoreRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

       /**
     * Index resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $categories = Category::get();

        return response()->json([
            'data' => $categories
        ]);
    }

    /**
     * Get single resource
     *
     * @param Category $Category
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Category $category ) {

        return response()->json([
            'data' => $category
        ]);
    }
    /**
     * Update single resource
     *
     * @param CategoryStoreRequest $request
     * @param Category $Category
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( CategoryStoreRequest $request, Category $category ) {
        $category->fill($request->all());
        $category->save();

        return response()->json([
            'status' => true,
            'data' => $category
        ]);
    }

    /**
     * Store new resource
     *
     * @param ClientStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( CategoryStoreRequest $request ) {
        $category = new Category;
        $category->fill($request->all());
        $category->save();

        return response()->json([
            'status' => true,
                'data' => [
                'id' => $category->id
            ]
        ]);
    }

    /**
     * Destroy single resource
     *
     * @param Category $category
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( Category $category ) {
        $category->delete();

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * Destroy resources by ids
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroyMass( Request $request ) {
        $request->validate([
            'ids' => 'required|array'
        ]);

        Category::destroy($request->ids);

        return response()->json([
            'status' => true
        ]);
    }
}
