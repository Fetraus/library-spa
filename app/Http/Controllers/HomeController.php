<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use App\Client;
use App\ClientBook;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function data(){
        $data = [];
        $data ['number_of_clients'] = Client::count();
        $data ['number_of_books'] = Book::count();
        $data ['number_of_borrows'] = ClientBook::count();
        return response()->JSON($data);

    }

}
