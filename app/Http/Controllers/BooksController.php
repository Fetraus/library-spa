<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\Client;
use Spatie\Searchable\Search;
use App\Http\Requests\BookStoreRequest;
use Illuminate\Http\Request;
use Spatie\Searchable\ModelSearchAspect;

class BooksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


     public function index (Request $request)
    {
        $books = Book::with('categories')->get();

        return response()->json([
            'data' => $books
            ]);
    }
    /**
     * Index resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request) {
        $books = (new Search())

        ->registerModel(Book::class, function(ModelSearchAspect $modelSearchAspect) {
            $modelSearchAspect
               ->addSearchableAttribute('name') // return results for partial matches on usernames
               ->addSearchableAttribute('author') //
               ->with('categories');
     })
        ->search($request->input('query'));
        // dd($books);

        return response()->json([
            'data' => $books
        ]);
    }

    /**
     * Get single resource
     *
     * @param Book $book
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Book $book ) {
        $data=$book->with('categories')->where('id',$book->id)->first()->toArray();
        // dd($data);
        return response()->json([
            'data' => $data
        ]);
    }
    public function update( BookStoreRequest $request, Book $book ) {
        $book->fill($request->all());
        $book->save();
        $book->categories()->sync($request->get('category'));


        return response()->json([
            'status' => true,
            'data' => $book->with('categories')->where('id',$book->id)->first()->toArray()
        ]);
    }
    public function store( BookStoreRequest $request ) {
        $book = new Book;
        $book->fill($request->all());
        $book->save();
        $book->categories()->sync($request->get('category'));

        return response()->json([
            'status' => true,
            // 'created' => true,
            'data' => [
                'id' => $book->id
            ]
        ]);
    }
    /**
     * Destroy single resource
     *
     * @param Book  $book
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( Book $book ) {
        $book->delete();

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * Destroy resources by ids
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroyMass( Request $request ) {
        $request->validate([
            'ids' => 'required|array'
        ]);

        Book::destroy($request->ids);

        return response()->json([
            'status' => true
        ]);
    }
    public function borrow (Request $request) {
        $request->validate([
            'clientId' => 'required|exists:clients,id',
            'bookIds' => 'required|array',
            'bookIds.*' => 'exists:books,id'
        ]);
        $client = Client::find($request->get('clientId'));
        $bookIds = $request->get('bookIds');
        Book::whereIn('id', $bookIds)->update(['availability'=>false]);

        $date = date('Y-m-d H:i:s');
        $client->books()->attach($bookIds,['created_at'=>$date, 'updated_at'=>$date]);
    }
}
