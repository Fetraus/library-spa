<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientBook;

class ClientBookController extends Controller
{

    public function index(){
        $clientbook = ClientBook::get();
        dd($clientbook);
    }

    /**
     * Destroy single relationship
     *
     * @param ClientBook $clientbook
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( ClientBook $clientbook ) {
        $clientbook->delete();

        return response()->json([
            'status' => true
        ]);
    }
}
