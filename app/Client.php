<?php

namespace App;

use App\Processors\AvatarProcessor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Client extends Model implements Searchable
{
    public function books(){
        return $this->belongsToMany(Book::class,'clients_books');
    }

    public function borrowedBooks(){
        return $this->belongsToMany(Book::class,'clients_books')->withPivot('id')->wherePivotNull('deleted_at');
    }

    public function getSearchResult(): SearchResult
    {       $url = route('clients.show', $this->id);

       return new SearchResult($this, $this->name, $url);
    }

    public function getBorrowedBooksAttribute() {
        return $this->borrowedBooks()->with('categories')->get();
    }
    protected $appends = [
        'created'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'file_id',
        'file'
    ];

    protected $fillable = [
        'name',
        'company',
        'city',
        'progress',
        'created_date',
        'file_id'
    ];

    public function file() {
        return $this->belongsTo(File::class);
    }

    public function getAvatarFilenameAttribute() {
        if (!empty($this->file)) {
            return $this->file->name;
        }

        return null;
    }

    public function getAvatarAttribute() {
        return AvatarProcessor::get($this);
    }

    public function getCreatedAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->toFormattedDateString();
    }

    public function getCreatedMmDdYyyyAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->format('m-d-Y');
    }

    public function setCreatedDateAttribute( $value ) {
        try {
            $this->attributes['created_at'] = new Carbon($value);
        } catch (\Exception $exception) {
            $this->attributes['created_at'] = now();
        }
    }
}
