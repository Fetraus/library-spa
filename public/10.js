(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CartTableSample.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CartTableSample.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_ModalTrashBox__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/ModalTrashBox */ "./resources/js/components/ModalTrashBox.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'CartTableSample',
  components: {
    ModalTrashBox: _components_ModalTrashBox__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    dataUrl: {
      type: String,
      "default": null
    },
    checkable: {
      type: Boolean,
      "default": false
    },
    filter: {
      type: String,
      "default": null
    }
  },
  data: function data() {
    return {
      isModalActive: false,
      trashObject: null,
      books: this.$store.state.cart,
      isLoading: false,
      paginated: false,
      perPage: 10,
      checkedRows: []
    };
  },
  watch: {
    '$store.state.cart': function $storeStateCart() {
      this.books = this.$store.state.cart;

      if (this.books.length > this.perPage) {
        this.paginated = true;
      }

      ;
    }
  },
  computed: {
    trashObjectName: function trashObjectName() {
      if (this.trashObject) {
        return this.trashObject.name;
      }

      return null;
    }
  },
  // created () {
  //   this.getData()
  // },
  methods: {
    // getData () {
    //   if (this.dataUrl) {
    //     this.isLoading = true
    //     var activeUrl = this.dataUrl
    //     if (this.filter){
    //       const searchParams = new URLSearchParams({
    //       query:this.filter
    //     });
    //       activeUrl = activeUrl+'?'+searchParams.toString()
    //     }
    //     axios
    //       .get(activeUrl)
    //       .then(r => {
    //         this.isLoading = false
    //         if (r.data && r.data.data) {
    //           if (r.data.data.length > this.perPage) {
    //             this.paginated = true
    //           }
    //           if (this.filter){
    //             this.clients = r.data.data.map(item=>item.searchable)
    //           }
    //           else {
    //           this.clients = r.data.data
    //         }
    //       }
    //       })
    //       .catch( err => {
    //         this.isLoading = false
    //         this.$buefy.toast.open({
    //           message: `Error: ${e.message}`,
    //           type: 'is-danger',
    //           queue: false
    //         })
    //       })
    //   }
    // },
    removeFromCart: function removeFromCart(book) {
      this.$store.commit('removeFromCart', book);
    },
    trashModal: function trashModal(trashObject) {
      this.trashObject = trashObject;
      this.isModalActive = true;
    },
    trashConfirm: function trashConfirm() {
      var _this = this;

      this.isModalActive = false;
      axios["delete"]("/clients/".concat(this.trashObject.id, "/destroy")).then(function (r) {
        _this.getData();

        _this.$buefy.snackbar.open({
          message: "Deleted ".concat(_this.trashObject.name),
          queue: false
        });
      })["catch"](function (err) {
        _this.$buefy.toast.open({
          message: "Error: ".concat(err.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    trashCancel: function trashCancel() {
      this.isModalActive = false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Books.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Books.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_Notification__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/Notification */ "./resources/js/components/Notification.vue");
/* harmony import */ var _components_CartTableSample__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/CartTableSample */ "./resources/js/components/CartTableSample.vue");
/* harmony import */ var _components_BooksTableSample__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/BooksTableSample */ "./resources/js/components/BooksTableSample.vue");
/* harmony import */ var _components_ClientsTableSample__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/ClientsTableSample */ "./resources/js/components/ClientsTableSample.vue");
/* harmony import */ var _components_CardComponent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/components/CardComponent */ "./resources/js/components/CardComponent.vue");
/* harmony import */ var _components_TitleBar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/components/TitleBar */ "./resources/js/components/TitleBar.vue");
/* harmony import */ var _components_HeroBar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/components/HeroBar */ "./resources/js/components/HeroBar.vue");
var _name$name$components;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = (_name$name$components = {
  name: 'Books'
}, _defineProperty(_name$name$components, "name", 'Clients'), _defineProperty(_name$name$components, "components", {
  HeroBar: _components_HeroBar__WEBPACK_IMPORTED_MODULE_6__["default"],
  TitleBar: _components_TitleBar__WEBPACK_IMPORTED_MODULE_5__["default"],
  CardComponent: _components_CardComponent__WEBPACK_IMPORTED_MODULE_4__["default"],
  BooksTableSample: _components_BooksTableSample__WEBPACK_IMPORTED_MODULE_2__["default"],
  ClientsTableSample: _components_ClientsTableSample__WEBPACK_IMPORTED_MODULE_3__["default"],
  CartTableSample: _components_CartTableSample__WEBPACK_IMPORTED_MODULE_1__["default"],
  Notification: _components_Notification__WEBPACK_IMPORTED_MODULE_0__["default"]
}), _defineProperty(_name$name$components, "data", function data() {
  return {
    queryBooks: null,
    queryClients: null,
    results: []
  };
}), _defineProperty(_name$name$components, "computed", {
  titleStack: function titleStack() {
    return ['Workspace', 'Borrow'];
  }
}), _defineProperty(_name$name$components, "methods", {
  checkoutBooks: function checkoutBooks() {
    var _this = this;

    var data = {
      clientId: this.$store.state.client.id,
      bookIds: this.$store.state.cart.map(function (a) {
        return a.id;
      })
    };
    axios.post("/books-checkout", data).then(function (r) {
      _this.$store.commit('clearCartAfterCheckout');
    })["catch"](function (r) {});
  }
}), _name$name$components);

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CartTableSample.vue?vue&type=template&id=9dd93786&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CartTableSample.vue?vue&type=template&id=9dd93786& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("modal-trash-box", {
        attrs: {
          "is-active": _vm.isModalActive,
          "trash-subject": _vm.trashObjectName
        },
        on: { confirm: _vm.trashConfirm, cancel: _vm.trashCancel }
      }),
      _vm._v(" "),
      _c(
        "b-table",
        {
          attrs: {
            "checked-rows": _vm.checkedRows,
            checkable: _vm.checkable,
            loading: _vm.isLoading,
            paginated: _vm.paginated,
            "per-page": _vm.perPage,
            striped: true,
            hoverable: true,
            "default-sort": "name",
            data: _vm.books
          },
          on: {
            "update:checkedRows": function($event) {
              _vm.checkedRows = $event
            },
            "update:checked-rows": function($event) {
              _vm.checkedRows = $event
            }
          },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(props) {
                return [
                  _c(
                    "b-table-column",
                    { staticClass: "has-no-head-mobile is-image-cell" },
                    [
                      props.row.avatar
                        ? _c("div", { staticClass: "image" }, [
                            _c("img", {
                              staticClass: "is-rounded",
                              attrs: { src: props.row.avatar }
                            })
                          ])
                        : _vm._e()
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { label: "Name", field: "name", sortable: "" } },
                    [_vm._v("\n        " + _vm._s(props.row.name) + "\n      ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    {
                      attrs: { label: "Author", field: "author", sortable: "" }
                    },
                    [
                      _vm._v(
                        "\n        " + _vm._s(props.row.author) + "\n      "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    {
                      attrs: {
                        label: "Published Date",
                        field: "published_date",
                        sortable: ""
                      }
                    },
                    [
                      _vm._v(
                        "\n        " +
                          _vm._s(props.row.published_date) +
                          "\n      "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    {
                      attrs: {
                        label: "Publisher",
                        field: "publisher",
                        sortable: ""
                      }
                    },
                    [
                      _vm._v(
                        "\n        " + _vm._s(props.row.publisher) + "\n      "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    {
                      attrs: {
                        label: "Category",
                        field: "category",
                        sortable: ""
                      }
                    },
                    [
                      _vm._v(
                        "\n        " +
                          _vm._s(
                            props.row.categories
                              .map(function(a) {
                                return a.name
                              })
                              .join(", ")
                          ) +
                          "\n      "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    {
                      staticClass: "is-actions-cell",
                      attrs: { "custom-key": "actions" }
                    },
                    [
                      _c("div", { staticClass: "buttons is-right" }, [
                        _c(
                          "button",
                          {
                            staticClass: "button is-small is-danger",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.removeFromCart(props.row)
                              }
                            }
                          },
                          [
                            _c("b-icon", {
                              attrs: { icon: "trash-can", size: "is-small" }
                            })
                          ],
                          1
                        )
                      ])
                    ]
                  )
                ]
              }
            }
          ])
        },
        [
          _vm._v(" "),
          _c(
            "section",
            { staticClass: "section", attrs: { slot: "empty" }, slot: "empty" },
            [
              _c(
                "div",
                { staticClass: "content has-text-grey has-text-centered" },
                [
                  _vm.isLoading
                    ? [
                        _c(
                          "p",
                          [
                            _c("b-icon", {
                              attrs: {
                                icon: "dots-horizontal",
                                size: "is-large"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("p", [_vm._v("Fetching data...")])
                      ]
                    : [
                        _c(
                          "p",
                          [
                            _c("b-icon", {
                              attrs: { icon: "emoticon-sad", size: "is-large" }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("p", [_vm._v("Nothing's here…")])
                      ]
                ],
                2
              )
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Books.vue?vue&type=template&id=19aa37ca&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Books.vue?vue&type=template&id=19aa37ca& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("title-bar", { attrs: { "title-stack": _vm.titleStack } }),
      _vm._v(" "),
      _c(
        "hero-bar",
        [
          _vm._v("\n    Borrow\n    "),
          _c(
            "router-link",
            {
              staticClass: "button",
              attrs: { slot: "right", to: "/" },
              slot: "right"
            },
            [_vm._v("\n      Dashboard\n    ")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "section",
        { staticClass: "section is-main-section" },
        [
          _c(
            "card-component",
            {
              staticClass: "has-table has-mobile-sort-spaced",
              attrs: { title: "Books", icon: "book-open-variant" }
            },
            [
              _c("div", { staticClass: "control" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.queryBooks,
                      expression: "queryBooks"
                    }
                  ],
                  staticClass: "input",
                  attrs: { type: "text", placeholder: "Search everywhere..." },
                  domProps: { value: _vm.queryBooks },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.queryBooks = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("books-table-sample", {
                attrs: {
                  filter: this.queryBooks,
                  "data-url": "/books",
                  "search-url": "/books/search",
                  checkable: true
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c(
            "card-component",
            {
              staticClass: "has-table has-mobile-sort-spaced",
              attrs: { title: "Clients", icon: "book-open-variant" }
            },
            [
              _c("div", { staticClass: "control" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.queryClients,
                      expression: "queryClients"
                    }
                  ],
                  staticClass: "input",
                  attrs: { type: "text", placeholder: "Search everywhere..." },
                  domProps: { value: _vm.queryClients },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.queryClients = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("clients-table-sample", {
                attrs: {
                  filter: this.queryClients,
                  "data-url": "/clients",
                  checkable: false,
                  focusable: true
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c(
            "card-component",
            {
              staticClass: "has-table has-mobile-sort-spaced",
              attrs: { title: "Cart", icon: "book-open-variant" }
            },
            [
              _c("cart-table-sample", {
                attrs: { "data-url": "", checkable: true }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "buttons" }, [
            _c(
              "button",
              {
                staticClass: "button  is-info",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.checkoutBooks()
                  }
                }
              },
              [
                _c("b-icon", {
                  attrs: { icon: "cart-plus", "custom-size": "default" }
                }),
                _vm._v(" "),
                _c("span", [_vm._v("Checkout")])
              ],
              1
            )
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/CartTableSample.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/CartTableSample.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CartTableSample_vue_vue_type_template_id_9dd93786___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CartTableSample.vue?vue&type=template&id=9dd93786& */ "./resources/js/components/CartTableSample.vue?vue&type=template&id=9dd93786&");
/* harmony import */ var _CartTableSample_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CartTableSample.vue?vue&type=script&lang=js& */ "./resources/js/components/CartTableSample.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CartTableSample_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CartTableSample_vue_vue_type_template_id_9dd93786___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CartTableSample_vue_vue_type_template_id_9dd93786___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/CartTableSample.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/CartTableSample.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/CartTableSample.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CartTableSample_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./CartTableSample.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CartTableSample.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CartTableSample_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/CartTableSample.vue?vue&type=template&id=9dd93786&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/CartTableSample.vue?vue&type=template&id=9dd93786& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CartTableSample_vue_vue_type_template_id_9dd93786___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./CartTableSample.vue?vue&type=template&id=9dd93786& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CartTableSample.vue?vue&type=template&id=9dd93786&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CartTableSample_vue_vue_type_template_id_9dd93786___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CartTableSample_vue_vue_type_template_id_9dd93786___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/Books.vue":
/*!**************************************!*\
  !*** ./resources/js/views/Books.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Books_vue_vue_type_template_id_19aa37ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Books.vue?vue&type=template&id=19aa37ca& */ "./resources/js/views/Books.vue?vue&type=template&id=19aa37ca&");
/* harmony import */ var _Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Books.vue?vue&type=script&lang=js& */ "./resources/js/views/Books.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Books_vue_vue_type_template_id_19aa37ca___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Books_vue_vue_type_template_id_19aa37ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Books.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Books.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/views/Books.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Books.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Books.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Books.vue?vue&type=template&id=19aa37ca&":
/*!*********************************************************************!*\
  !*** ./resources/js/views/Books.vue?vue&type=template&id=19aa37ca& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_template_id_19aa37ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Books.vue?vue&type=template&id=19aa37ca& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Books.vue?vue&type=template&id=19aa37ca&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_template_id_19aa37ca___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_template_id_19aa37ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);